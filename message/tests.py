from django.test import TestCase

from django.test import Client
from django.urls import resolve

# Unit Testing
from .views import index, confirm
from .models import Message
from .forms import MessageForm

from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options


class Story7UnitTest(TestCase):

    def test_url_index_is_exist(self):
        response = Client().get('/')
        self.assertEqual(response.status_code, 200)
    
    def test_url_confirm_is_exist(self):
        response = Client().post('/confirm/', {
            'name':"name", 
            'message':"message"
            })
        self.assertEqual(response.status_code, 200)

    def test_views_index_func_used(self):
        found = resolve('/')
        self.assertEqual(found.func, index)
    
    def test_views_confirm_func_used(self):
        found = resolve('/confirm/')
        self.assertEqual(found.func, confirm)

    def test_template_index_used(self):
        response = Client().get('/')
        self.assertTemplateUsed(response, 'landing.html')

    def test_template_confirm_used(self):
        response = Client().post('/confirm/', {'name':'HazLazuardi', 'message':"Yuhu"})
        self.assertTemplateUsed(response, 'confirm.html')

    def test_model_create_new_status(self):
        Message.objects.create(
            name='HazLazuardi', 
            message='FirstTimeNie'
            )
        status_count = Message.objects.all().count()
        self.assertEqual(status_count, 1)

    def test_form_blank_item_validation(self):
        form = MessageForm(data={
            'name':'',
            'message':''
            })
        self.assertFalse(form.is_valid())
        self.assertEqual(
            form.errors['name'],
            ["This field is required."]
        )
        self.assertEqual(
            form.errors['message'],
            ["This field is required."]
        )

    def test_post_success_and_render_the_result(self):
        test = 'HazLazuardi'
        response_post = Client().post('/', {
            'name':test, 
            'message':"Muncul ga"
            })
        self.assertEqual(response_post.status_code, 200)
        response= Client().get('/')
        html_response = response.content.decode('utf8')
        self.assertIn(test, html_response)

