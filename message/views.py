from django.shortcuts import render

from .models import Message
from .forms import MessageForm
from django.db.models import Q
from django.http import HttpResponseRedirect
from django.shortcuts import get_object_or_404, render


# Create your views here.

def index(request):
    if(request.method == "POST"):
        name = request.POST['name']
        message = request.POST['message']
        Message.objects.create(name=name, message=message)
        qs = Message.objects.all()[:6]
        context = {
        'queryset' : qs,
        'form' : MessageForm()
        }
        return render(request, "landing.html", context=context)
    else:
        qs = Message.objects.all()[:6]
        context = {
        'queryset' : qs,
        'form' : MessageForm()
        }
        return render(request, "landing.html", context=context)

def confirm(request):
    if(request.method == "POST"):
        name = request.POST['name']
        message = request.POST['message']
        context = {
            'name': name,
            'message': message,
        }
        return render(request, "confirm.html", context=context)